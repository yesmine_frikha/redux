import counterReducer from "./counter";
import LoggedReducer from "./logged";
import { combineReducers } from "redux";

const allReducer = combineReducers({
  counter: counterReducer,
  isLogged: LoggedReducer,
});
export default allReducer;
